console.log("Game loaded...");

// Variabelen declareren
var animations, 
    animationEndings,
    buttons, 
    mario, 
    energyBar, 
    LowestEnergyCost, 
    currentEnergy, 
    animationStarted, 
    noEnergy,
    maxEnergy,
    energyOutput,
    star,
    snoring,
    timer,
    pause,
    streak,
    streakOutput,
    streakRecordOutput,
    difficultSettings,
    myDifficulty,

// Waardes toewijzen aan gedeclareerde variabelen
buttons = document.querySelectorAll('.mario section > button');
mario = document.querySelector('.mario');
energyBar = document.querySelector('.energy span');
currentEnergy = energyBar.offsetWidth;
noEnergy = false;
maxEnergy = energyBar.dataset.max_energy;
energyOutput = document.querySelector('.energy > span > span');
streakOutput = document.querySelector('.streak > span:first-child > span');
streakRecordOutput = document.querySelector('.streak > span:last-child > span');
energyOutput.textContent = maxEnergy;
animationStarted = false;
stars = [];
snoring = playSound('snoring', 1, false);
timer = {
    energy: false,
    sleepyness: false,
}
animations = [];
animationEndings = [];
pause = false;
streak = {
    count : 0,
    record: 0,
    lastClicked : 0,
    reset : false,
    activeBonus : false,
}

// Voor elk niveau een streak count opstellen voor het aantal bonuspunten
difficultSettings = {
    easy: {
        streaks: {
            low: {
                count: 3,
                bonus: 1,
            },
            mid: {
                count: 6,
                bonus: 3,
            },
            high: {
                count: 9,
                bonus: 6,
            },
        },
        click_speed : 2.2,
        stars: 12
    },
    normal: {
        streaks: {
            low: {
                count: 4,
                bonus: 2,
            },
            mid: {
                count: 8,
                bonus: 5,
            },
            high: {
                count: 12,
                bonus: 9,
            },
        },
        click_speed : 1.6,
        stars: 9
    },
    hard: {
        streaks: {
            low: {
                count: 6,
                bonus: 3,
            },
            mid: {
                count: 12,
                bonus: 8,
            },
            high: {
                count: 18,
                bonus: 12,
            },
        },
        click_speed : 1,
        stars: 6
    },
}

// Haal de streak record op als deze bestaat en anders is hij 0
function getStreakRecord(){

    let checkRecord = getCookie('streak_record');
    if(!checkRecord == false){
        record = checkRecord;
    } else {
        setCookies('streak_record', 0);
        record = streak.count;
    }

    streak.record = record;
    streakRecordOutput.textContent = streak.record;
}

// Krijg de laagste prijs van alle animaties
function getLowestEnergyCost(){

    animations.sort(function (a, b) {
        return a.cost - b.cost
    });
    LowestEnergyCost = animations[0].cost;
}

// Zorgt voor het juiste aantal sterren aan de hand van het gekozen niveau
function cloneStars(){

    let star = document.querySelector('#og-star');
    for (let i = 0; i < myDifficulty.stars; i++) {
        cln = star.cloneNode(true);
        stars.push(cln);
        mario.appendChild(cln);      
    }

    stars.forEach(star => {
        star.addEventListener('click', handleStar);
    });

    star.remove();
}

// Zorgt ervoor dat er bij de energybar een bepaald aantal punten bijkomt of af gaat
function handleEnergy(cost, type){

    switch (type) {
        case 'add':
            currentEnergy = currentEnergy + cost;
            break;
    
        default:
            currentEnergy = currentEnergy - cost;
            break;
    }
    energyBar.style.width = `${currentEnergy}px`;
    toggleButtons('select');
    updateEnergyOutput();
}

// Haal een animation op uit de animations array bij zijn naam
function getAnimation(name) {
    for (var i=0; i < animations.length; i++){
        if (animations[i]['name'] == name){
            return animations[i];
        }
    }
}

// Activeert of deactiveert animatiebuttons
function toggleButtons(type, disabled){

    if(type == 'all'){
        buttons.forEach(button => {
            button.disabled = disabled; 
        });
    } else if(type == 'select' && !animationStarted){

        buttons.forEach(button => {
            if(button.dataset.cost > currentEnergy){
                button.disabled = true; 
            } else {
                button.disabled = false; 
            }
        });
    }
}

// Controleert of all buttons disabled moeten zijn als er een animatie aan de gang is
function checkButtons(e){

    let endingCheck = true;
    let animation = getAnimation(e.animationName);
    if(!animation){
        endingCheck = animationEndings.includes(e.animationName);
    }

    if(e.type == 'animationstart' || (e.type == 'animationend' && endingCheck)){
        let disabled = (e.type == 'animationstart') ? true : false;
        toggleButtons('all', disabled);
    }
}

// Afspelen van animaties met type 1 of 2 en ervoor zorgen dat het juiste aantal punten van de energy af gaat
function handleButton(e){

    animationStarted = true;
    if(e.type == 1){

        playSound(e.name, e.sfx_speed, true, volumes.animations);
        mario.classList.add(e.name); 
        mario.onanimationend = () => {

            mario.classList.remove(e.name);
            animationStarted = false;
        };
    } else {
        
        playSound(e.name, 1, true, volumes.animations);
        mario.classList.add(e.name); 
        mario.onanimationend = (o) => {
            if(o.animationName == e.ending){

                mario.classList.remove(e.name);
                animationStarted = false;
            }
        };
    }
    
    handleEnergy(e.cost);
}

// Krijg een random getal terug binnen de borders van je viewport
const getRandom = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);

// Geef de sterren een random location on load of als er op eentje geklikt wordt
function randomLocation(type, e = null){
    
    if(type == 'all'){

        stars.forEach(star => {
            star.style.left = getRandom(0, window.innerWidth - 150) +'px';        
            star.style.top = getRandom(0, window.innerHeight - 150) +'px';
        });
    } else {
        
        e.target.parentNode.style.left = getRandom(0, window.innerWidth - 150) +'px';        
        e.target.parentNode.style.top = getRandom(0, window.innerHeight - 150) +'px';
    }
}

/* 
Controleer de streak en haal dan het correct aantal punten op om eraf te halen
en daarnaast ook het correcte geluid afspelen en de ster een random locatie geven
*/
function handleStar(e){
    checkStreak();
    
    let energyFill = 10 + checkBonus();
    energyFill = (currentEnergy + energyFill > maxEnergy) ? (maxEnergy - currentEnergy) : energyFill;

    if(currentEnergy < maxEnergy){
        displayStarPoint(e.target.parentNode, energyFill);

        playSound('star_ding', 1.5, true, volumes.stars);
        handleEnergy(energyFill, 'add');
        randomLocation('single', e);
    } else {
        
        playSound('energy_full', 1.5, true, volumes.stars);
    }
}

// Wanneer er op een ster wordt geklikt laten weten hoeveel punten deze opleverde, inclusief bonus
function displayStarPoint(star, energyfill){

    let span; 
    let spanCheck = document.querySelector('.mario > span'); 
    if(!spanCheck){
        
        span = document.createElement("span");
    } else {
        span = spanCheck;
    }

    span.style.left = star.style.left;  
    span.style.top = star.style.top;    
    span.innerHTML = '+' + energyfill;  
    span.classList.add('display-point');
    star.parentNode.appendChild(span); 
}

// Haal de huidge niveau settings op als deze bestaan en anders haal de default settings op
function getDifficultySettings(){
    myDifficulty = (!difficultSettings[getCookie('difficulty')] == false) 
    ? difficultSettings[getCookie('difficulty')] 
    : difficultSettings[difficulty];
}

// Controleer op de juiste bonuspunten aan de hand van het gekozen niveau
function checkBonus(){

    let bonus;
    
    if(streak.count >= myDifficulty.streaks.high.count){

        streak.activeBonus = true;
        bonus = myDifficulty.streaks.high.bonus;
    } else if(streak.count >= myDifficulty.streaks.mid.count){

        bonus = myDifficulty.streaks.mid.bonus;
    } else if(streak.count >= myDifficulty.streaks.low.count){

        bonus = myDifficulty.streaks.low.bonus; 
    } else {

        streak.activeBonus = false;
        bonus = 0; 
    }
    return bonus;
}

// Kijkt hoelang het mag duren voordat je huidige streak verdwijnt
function checkStreak() {

    let clickSpeed = myDifficulty.click_speed * 1000;
    var timeNow = (new Date()).getTime();

    if (timeNow < (streak.lastClicked + clickSpeed) || ( streak.reset == false && streak.count == 0)) {
        streak.count++;
    } else {
        streak.reset = true;
        streak.count = 1;
    }
    streakOutput.textContent = streak.count;

    if(streak.count > streak.record){
        streak.record = streak.count;
        streakRecordOutput.textContent = streak.record;
        setCookies('streak_record', streak.count);
    }
    streak.lastClicked = timeNow;
}

/* 
    Controleert de animatiebuttons op welke animatie afgespeeld moet worden
    en daarnaast worden de resterende buttons gedeactiveerd als de animatie zich afspeelt
*/
function checkButton(e){
        
    mario.addEventListener("animationstart", checkButtons);
    mario.addEventListener("animationend", checkButtons);

    const action = e.target.dataset.name;
    let animation = getAnimation(action);
    
    if((currentEnergy - animation.cost) >= 0){

        if(animation.type == 1 || animation.type == 2){

            handleButton(animation);
            
        } else if(action == "scary"){

            animationStarted = true;

            const animations = [
                'scary_eyecolor',
                'move_scary_eyes',
                'scary_bg',
            ];

            handleEnergy(animation.cost);

            mario.classList.add("scary", animations[0]);        
            mario.addEventListener("animationend", function(e){
                if(animations.includes(e.animationName)){
                    let key = animations.indexOf(e.animationName);
                    const nextKey = key + 1;
                    setAnimationClass([animations[key], animations[nextKey]]);
                }

                if(e.animationName == 'scary_eyecolor'){
                    playSound('cracking', 1.6, true, volumes.animations);
                }

                if(e.animationName == 'move_scary_eyes'){
                    playSound('scary', 1.3, true, volumes.animations);                
                }
            }, false);

            let setAnimationClass = (animation) => {

                mario.classList.remove(animation[0]);
                if(typeof animation[1] != 'undefined'){ 

                    mario.classList.add(animation[1])
                } else {

                    mario.classList.remove('scary');
                    animationStarted = false;
                };
            }
        }        
    }
}

// Word elke seconde gecalled om het tick geluid af te spelen en haalt 10 of minder als punten weg van de energybar 
function drainEnergy(){
    
    if(!pause){

        let cost = (energyBar.offsetWidth > 10) ? 10 : energyBar.offsetWidth;
        handleEnergy(cost);

        if(currentEnergy > 0){
            playSound('energy_ticking', 1, true, volumes.ticking);
        }
    }
}

// Pauseren van de timers
function pauseGame(type = 'all'){

    pause = true;
    if(type == 'all'){
        
        clearInterval(timer.energy);
        clearInterval(timer.sleepyness);
        timer.energy = 0;
        timer.sleepyness = 0;
    } else if(type == 'energy'){
        
        clearInterval(timer.energy);
        timer.energy = 0;
    } else if(type == 'sleepyness'){
        
        clearInterval(timer.sleepyness);
        timer.sleepyness = 0;
    }
}

// starten van de timers
function playGame(type = 'all'){
    
    pause = false;
    if(type == 'all'){
        
        timer.energy = setInterval(drainEnergy, 1000);
        timer.sleepyness = setInterval(checkSleepyness, 1000);
    } else if(type == 'energy'){
        
        timer.energy = setInterval(drainEnergy, 1000);
    } else if(type == 'sleepyness'){
        
        timer.sleepyness = setInterval(checkSleepyness, 1000);
    }
}

// Bekijken wanneer de energytimer uit of aan moet
function drainEnergyTimer(){

    if(energyBar.offsetWidth <= 10){

        pauseGame('energy');
    } else {
        if(!timer.energy) {
            playGame('energy');
        }
    }
}

// Zorgt ervoor dat mario moe is of moet slapen wanneer de energybar onder een bepaalt aantal punten komt
function checkSleepyness() {

    if(!pause){
        if(energyBar.offsetWidth > LowestEnergyCost && energyBar.offsetWidth < 45){
            
            if(mario.classList.contains('sleep')){
                mario.classList.remove('sleep');
            }
            mario.classList.add('tired');
        } else if(energyBar.offsetWidth < LowestEnergyCost){
            
            if(mario.classList.contains('tired')){
                mario.classList.remove('tired');
            }

            mario.classList.add('sleep');
            
            setTimeout(() => {
                snoring.volume = volumes.snore
                snoring.play();
            }, 1000);

        } else {

            snoring.currentTime = 0;
            snoring.pause();

            if(mario.classList.contains('tired')){
                mario.classList.remove('tired');
            }

            if(mario.classList.contains('sleep')){
                mario.classList.remove('sleep');
            }
        }
    }
}

// Bekijken wanneer de sleepyness uit of aan moet
function checkSleepynessTimer(){

    if(currentEnergy < LowestEnergyCost){
        
        playGame('sleepyness');
    } else {
        if(!timer.sleepyness) {
            pauseGame('sleepyness');
        }
    }
}

// Huidige energie weergeven in de energybar
function updateEnergyOutput(){
    energyOutput.textContent = currentEnergy;
}

// Ervoor zorgen dat de energy output elke seconde wordt geupdate
function updateEnergyOutputTimer(){
    setInterval(updateEnergyOutput, 1000);
}

// Alle timers aanzetten
function startTimers(){
    drainEnergyTimer();
    checkSleepynessTimer();
    updateEnergyOutputTimer();
}


/* 
Alle animaties ophalen uit de animatiebuttons en opslaan in de animations array
Daarna op animatiebuttons een eventlistener toevoegen
*/
buttons.forEach(button => {     

    animations.push(button.dataset);
    if(button.dataset.ending){
        animationEndings.push(button.dataset.ending);
    }
    button.children[0].textContent = '-' + button.dataset.cost;
    button.addEventListener('click' , checkButton, false);
});

// Functions callen wanneer de dom geladen is
(() => {
    getStreakRecord();
    getDifficultySettings();
    getLowestEnergyCost();
    cloneStars();
    randomLocation('all');
    toggleButtons('select', true);
    startTimers();
    playGame();
})();