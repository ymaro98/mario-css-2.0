console.log('Menu loaded...');

// Variabelen declareren
var volumes,
    header,
    volumeSliders,
    menuBtn,
    menuOpen,
    body,
    menuItemBtns,
    diffButtons,
    difficulty, 
    currentLocation

// Waardes toewijzen aan gedeclareerde variabelen
header = document.querySelector('header');
volumeSliders = document.querySelectorAll('#volume > input');
menuBtn = document.querySelector('.menu-btn');
menuOpen = getCookie('menu_open') == 'true' ? true : false;
body = document.body;
menuItemBtns = document.querySelectorAll('header > section:first-child > button');
diffButtons = document.querySelectorAll('#difficulty > button');
difficulty = 'normal';
volumes = {
    animations : 0.7,
    snore : 0.5,
    stars : 0.5,
    ticking: 0.5
},
// https://stackoverflow.com/questions/4758103/last-segment-of-url-in-jquery
currentLocation = location.href.substring(location.href.lastIndexOf('/') + 1);

/* 
    Ervoor zorgen dat de wijzigingen van de volume sliders worden gecontroleerd
    En daarnaast deze waardes updaten
*/
function setSliderListeners(){
    volumeSliders.forEach(slider => {
        slider.addEventListener('change', setVolume);

        let volume;
        if(slider.dataset.name == 'animation'){

            
            if(!getCookie('volume_animation') == false){
                
                volume = getCookie('volume_animation');
                volumes.animations = volume;
                slider.value = volume * 100;
            } else {
                
                setCookies('volume_animation', volumes.animations);
                slider.value = volumes.animations * 100;
            }
        }

        if(slider.dataset.name == 'snore'){

            
            if(!getCookie('volume_snore') == false){
                
                volume = getCookie('volume_snore');
                volumes.snore = volume;
                slider.value = volume * 100;
            } else {

                setCookies('volume_snore', volumes.snore);
                slider.value = volumes.snore * 100;
            }
        }

        if(slider.dataset.name == 'stars'){

            
            if(!getCookie('volume_stars') == false){
                
                volume = getCookie('volume_stars');
                volumes.stars = volume;
                slider.value = volume * 100;
            } else {

                setCookies('volume_stars', volumes.stars);
                slider.value = volumes.stars * 100;
            }
        }

        if(slider.dataset.name == 'ticking'){

            
            if(!getCookie('volume_energyTick') == false){
                
                volume = getCookie('volume_energyTick');
                volumes.ticking = volume;
                slider.value = volume * 100;
            } else {

                setCookies('volume_energyTick', volumes.ticking);
                slider.value = volumes.ticking * 100;
            }
        }
    });
}

/* 
    Zorgt ervoor dat de game ook gepauseert kan worden met esc
*/
function keyPress (e) {
    if (e.keyCode == 27) {
        openMenu();
    }
}

// Een geluid afspelen met variable opties
function playSound(sfx, rate = 1, play = true, volume = 1){

    let sound = new Audio(`sounds/${sfx}.mp3`);
    
    sound.playbackRate = rate;
    sound.volume = volume;
    if(play){
        sound.play();
    }

    return sound;
}

// Volume aanpassen van een bepaald geluid
function setVolume(e){

    let name = e.target.dataset.name;
    let volume = e.target.value;
    let playNow = currentLocation != 'game.html' ? true : false;
    volume = volume / 100;
    switch(name) {

        case 'animation':

            volumes.animations = volume;
            playSound('yahoo', 1.2, playNow, volume);
            setCookies('volume_animation', volumes.animations);
            
            break;
            
        case 'snore':

            volumes.snore = volume;
            playSound('snoring', 1, playNow, volume);
            setCookies('volume_snore', volumes.snore);
                
            break;
                
        case 'stars':

            volumes.stars = volume;
            playSound('star_ding', 1.5, playNow, volume);
            setCookies('volume_stars', volumes.stars);

        case 'ticking':

            volumes.ticking = volume;
            playSound('energy_ticking', 1.5, playNow, volume);
            setCookies('volume_energyTick', volumes.ticking);
            
            break;
    
        default:
        break;
    }
}

// Het gekozen niveau uit het menu opslaan
function setDifficulty(e){

    let diff = e.target;
    if(diff.classList.contains('active')){

        playSound('diff_clicked');
    } else {

        playSound('diff_click');

        diffButtons.forEach(button => {
            button.classList.remove('active');
        });

        diff.classList.add('active');
        setCookies('streak_record', 0);
        location.reload();
    }
    
    setCookies('difficulty', diff.dataset.name);
    getDifficultySettings();
}

// Bekijken of het menu open is of niet
function checkOpenMenu(){

    if(menuOpen == true && currentLocation != 'game.html'){
        header.classList.add('open');
        menu.classList.add('active');
        body.classList.add('no-scroll');
    }   
}

// Opent of sluit het menu
function toggleMenu(){
    if(header.classList.contains('open')){

        if(currentLocation == 'game.html'){
            playGame();
        }

        menuOpen = false;
        header.classList.remove('open');
        menuBtn.classList.remove('active');
        body.classList.remove('no-scroll');
        playSound('menu_close', 1, true, 0.5);
        setCookies('menu_open', false);
    } else {
        
        if(currentLocation == 'game.html'){
            pauseGame();
        }
        
        menuOpen = true;
        header.classList.add('open');
        menuBtn.classList.add('active');
        body.classList.add('no-scroll');
        playSound('menu_open', 1, true, 0.1);
        setCookies('menu_open', true);
    }
}

// Naar een bepaalde pagina gaan
function goTo(e){
    let link =  e.target.dataset.location + '.html';

    if(currentLocation != link){
        
        setCookies('menu_open', false);
        location.href = link;
    } else {
        playSound('energy_full', 1, true, 0.5);
    }
}

// Opslaan van een cookie
function setCookies(cookie = '', value = ''){

    if(cookie !== '' && value !== ''){    
        document.cookie = cookie +'='+ value;
    }
}  

/* 
    Een cookie ophalen bij zijn naam
    Hulp van: https://www.tabnine.com/academy/javascript/how-to-set-cookies-javascript/
*/
function getCookie(cName) {
    const name = cName + "=";
    const cDecoded = decodeURIComponent(document.cookie);
    const cArr = cDecoded .split('; ');
    let res;
    cArr.forEach(val => {
        if (val.indexOf(name) === 0) res = val.substring(name.length);
    })
    return res;
}

// toggle menu wanneer de menu button wordt geklikt 
menuBtn.addEventListener('click', toggleMenu);
// Bekijk welke key wordt gedrukt 
body.addEventListener('keydown', keyPress);

// voor elke menu item button ervoor zorgen dat de functie goto wordt uitgevoerd
menuItemBtns.forEach(button => {
    button.addEventListener('click', goTo);
});

/* 
    Ervoor zorgen dat het huidge niveau en de bijpassende button ingedrukt is
*/
diffButtons.forEach(button => {
    button.addEventListener('click', setDifficulty);

    if(button.dataset.name == getCookie('difficulty')){

        button.classList.add('active');
    } else if(button.dataset.name == difficulty && !getCookie('difficulty')){

        button.classList.add('active');
    }
});

// Functions callen wanneer de dom geladen is
(() => {
    playSound('mario-boot', 1, true, 0);
    checkOpenMenu();
    setSliderListeners();
})();